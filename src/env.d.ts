/// <reference path="../.astro/types.d.ts" />
/// <reference types="astro/client" />

interface ImportMetaEnv {
  readonly AUTH_KEYCLOAK_ISSUER: string;
  readonly AUTH_KEYCLOAK_SECRET: string;
  readonly AUTH_KEYCLOAK_ID: string;
  readonly AUTH_TRUST_HOST: string;
  readonly PUBLIC_FC_INFRA_URL: string;
  readonly PUBLIC_FC_DATA_URL: string;
  readonly PUBLIC_DATA_ADAPTER_URL: string;
  readonly PUBLIC_INFRA_ADAPTER_URL: string;
  readonly PUBLIC_CREATION_WIZARD_API_URL: string;

  // more env variables...
}

// New type for public environment variables
type PublicEnv = {
  [K in keyof ImportMetaEnv as K extends `PUBLIC_${string}` ? K : never]: ImportMetaEnv[K];
};

interface ImportMeta {
  readonly env: ImportMetaEnv;
}

// Augment the Window interface
interface Window {
  envVars: PublicEnv;
}
declare namespace NodeJS {
  interface ProcessEnv extends ImportMetaEnv {}
}

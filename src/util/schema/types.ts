import type { JSONSchema4 } from 'json-schema';

export type RDFJsonSchema = JSONSchema4 & {
  rdfType: string;
};

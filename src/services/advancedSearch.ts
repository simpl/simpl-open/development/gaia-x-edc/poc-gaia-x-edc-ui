import type { ServiceShapesData } from 'types/shapes';

const ADVANCED_SEARCH_API_BASE_URL = 'https://xsfc-advsearch-be.dev.simpl-europe.eu';

// getAvailableShapesCategorized?ecosystem=simpl
// getJSON?ecosystem=simpl&name=Data%20Offering.json

export const getAvailableShapesCategorized = async () => {
  const response = await fetch(
    `${ADVANCED_SEARCH_API_BASE_URL}/getAvailableShapesCategorized?ecosystem=simpl`,
    {
      method: 'GET',
      headers: new Headers({
        Accept: 'application/json',
      }),
    }
  );

  const data = (await response.json()) as { Service: string[] };

  if (response.status === 200) {
    return data;
  } else {
    throw new Error();
  }
};

export const getShape = async (shapeName: string) => {
  const response = await fetch(
    `${ADVANCED_SEARCH_API_BASE_URL}/getJSON?ecosystem=simpl&name=${shapeName}`,
    {
      method: 'GET',
      headers: new Headers({
        Accept: 'application/json',
      }),
    }
  );

  const data = (await response.json()) as ServiceShapesData;
  if (response.status === 200) {
    return data;
  } else {
    throw new Error();
  }
};

export const getAllAvailableShapes = async () => {
  const shapesData = await getAvailableShapesCategorized();
  const shapeTypes = shapesData.Service;

  const shapes: ServiceShapesData[] = [];

  for (const shapeType of shapeTypes) {
    const data = await getShape(shapeType);
    shapes.push(data);
  }

  return shapes;
};

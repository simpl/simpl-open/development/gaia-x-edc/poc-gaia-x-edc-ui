import type { APIRoute } from 'astro';
import { checkSession } from '../services/keycloak';

// This route is used to keep the keycloak secret on server side, the env variable is only accessible there
export const POST: APIRoute = async ({ request }) => {
  const bearerToken = request.headers.get('Authorization');
  const token = bearerToken?.split('Bearer ')[1];

  if (!bearerToken || !token)
    return new Response(JSON.stringify({ error_description: 'Invalid token' }), {
      status: 401,
    });

  const isValidSession = await checkSession(token);

  if (!isValidSession)
    return new Response(JSON.stringify({ error_description: 'Invalid token' }), {
      status: 401,
    });

  return new Response(null, {
    status: 200,
  });
};

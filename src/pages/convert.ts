import { getPublicEnv } from '@/util/getEnv';
import { parseStream } from '@/util/ttlParser';
import formats from '@rdfjs/formats-common';
import type { Stream } from '@rdfjs/types';
import type { APIRoute } from 'astro';
import { Readable } from 'stream';

const { PUBLIC_FC_DATA_URL } = getPublicEnv();
// Function to read and parse the TTL file

// This route is used to keep the keycloak secret on server side, the env variable is only accessible there
export const GET: APIRoute = async ({ request }) => {
  const bearerToken = request.headers.get('Authorization');
  const token = bearerToken?.split('Bearer ')[1];

  const url = new URL(request.url);
  const params = Object.fromEntries(url.searchParams);

  if (!bearerToken || !token)
    return new Response(JSON.stringify({ error_description: 'Invalid token' }), {
      status: 401,
    });

  const schemaId = params.schemaId;

  if (!schemaId) {
    return new Response(JSON.stringify({ error_description: 'Missing schemaId parameter' }), {
      status: 400,
    });
  }

  const schemaFile = await fetch(`${PUBLIC_FC_DATA_URL}/schemas/` + schemaId, {
    method: 'GET',
    headers: {
      Authorization: `Bearer ${token}`,
      Accept: 'text/turtle',
    },
  });

  if (!schemaFile.ok) {
    return new Response(JSON.stringify({ error_description: 'Failed to fetch schema' }), {
      status: schemaFile.status,
    });
  }

  const schemaContent = await schemaFile.text();
  const input = Readable.from(schemaContent);

  // Usage example:

  const output: Stream = formats.parsers.import('text/turtle', input);

  const { root, prefixes } = await parseStream(output);

  return new Response(
    JSON.stringify({
      root,
      prefixes,
    }),
    {
      headers: {
        'content-type': 'application/json',
      },
    }
  );
};

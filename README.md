# POC Gaia-X EDC UI

## End of Life Notice

**IMPORTANT: This project is no longer maintained.**

As of 2024-10-24, this repository has reached its end of life and will no longer receive updates or support.

### Replacements

The functionality of this application has been split into two new applications, where development is going forward:

- [SIMPL Catalogue Client](https://code.europa.eu/simpl/simpl-open/development/gaia-x-edc/simpl-catalogue-client/)
- [SIMPL SD UI](https://code.europa.eu/simpl/simpl-open/development/gaia-x-edc/simpl-sd-ui)
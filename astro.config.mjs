import node from '@astrojs/node';
import react from '@astrojs/react';
import vue from '@astrojs/vue';
import { defineConfig } from 'astro/config';

import tailwind from '@astrojs/tailwind';
// import vuetify from 'vite-plugin-vuetify';

// astro.config.mjs
import vuetifyPlugin from 'vite-plugin-vuetify';

function vuetify(options) {
  return {
    name: 'my-astro-vuetify-integration',

    hooks: {
      'astro:config:setup': ({ updateConfig }) => {
        updateConfig({
          vite: {
            ssr: {
              noExternal: ['vuetify'],
            },

            plugins: [vuetifyPlugin()],
          },
        });
      },
    },
  };
}

/**
 * Vuetify integration for Astro
 * @param {import('astro/config').Options} options
 * @returns {import('astro/config').AstroIntegration}
 */

// https://astro.build/config
export default defineConfig({
  integrations: [
    vue({
      appEntrypoint: './src/pages/_app',
    }),
    vuetify(),
    react(),
    tailwind(),
  ],
  output: 'server',
  adapter: node({
    mode: 'standalone',
  }),
});

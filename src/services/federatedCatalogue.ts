import { getPublicEnv } from '@/util/getEnv';

const { PUBLIC_FC_DATA_URL, PUBLIC_DATA_ADAPTER_URL, PUBLIC_INFRA_ADAPTER_URL } = getPublicEnv();

export enum AdapterType {
  DATA = 'DATA',
  INFRA = 'INFRA',
}

type FetchSchemasResponse = {
  ontologies: string[];
  shapes: string[];
  vocabularies?: string[];
};

const getKeycloakToken = async () => {
  const token = window.localStorage.getItem('KEYCLOAK_ACCESS_TOKEN');
  if (!token) {
    throw new Error('No token found');
  }
  return token;
};

export const uploadSelfDescription = async (selfDescription: string, keycloakToken: string) => {
  const response = await fetch(`${PUBLIC_FC_DATA_URL}/self-descriptions`, {
    method: 'POST',
    headers: new Headers({
      Authorization: `Bearer ${keycloakToken}`,
      'Content-Type': 'application/json',
      Accept: 'application/json',
    }),
    body: selfDescription,
  });

  return response;
};

export const getSelfDescriptions = async (keycloakToken: string) => {
  const response = await fetch(`${PUBLIC_FC_DATA_URL}/self-descriptions`, {
    method: 'GET',
    headers: new Headers({
      Authorization: `Bearer ${keycloakToken}`,
      'Content-Type': 'application/json',
      Accept: 'application/json',
    }),
  });
  const data = await response.json();
  if (response.status === 200) {
    return data;
  } else {
    throw new Error();
  }
};

export interface FCSearchResult {
  title?: string;
  name?: string;
  description: string;
  [key: string]: unknown;
  claimsGraphUri: [string];
}

export interface RawCatalogSearchResults {
  items: { [key: string]: FCSearchResult }[];
  totalCount: number;
}

export interface CatalogSearchResults {
  items: FCSearchResult[];
  totalCount: number;
}

export interface CatalogSearchError {
  code: string;
  message: string;
}

const isError = (data: RawCatalogSearchResults | CatalogSearchError): data is CatalogSearchError =>
  (data as CatalogSearchError).code !== undefined &&
  (data as CatalogSearchError).message !== undefined;

const transformSearchResultItems = (data: RawCatalogSearchResults): FCSearchResult[] =>
  data.items.map((item) => Object.values(item)[0] as FCSearchResult);

export const searchCatalogue = async (
  catalogueUrl: string,
  statement: string,
  parameters?: Record<string, unknown>
): Promise<CatalogSearchResults> => {
  const response = await fetch(`${catalogueUrl}/query`, {
    method: 'POST',
    headers: new Headers({
      'Content-Type': 'application/json',
      Accept: 'application/json',
    }),
    body: JSON.stringify({ statement, parameters }),
  });

  const data: RawCatalogSearchResults | CatalogSearchError = await response.json();

  if (isError(data)) {
    throw new Error(data.message);
  }

  return {
    items: transformSearchResultItems(data),
    totalCount: data.totalCount,
  };
};

export const getSelfDescriptionById = async (id: string, token: string): Promise<any> => {
  const url = new URL(`${PUBLIC_FC_DATA_URL}/self-descriptions`);
  url.searchParams.append('ids', id);
  url.searchParams.append('withContent', 'true');
  url.searchParams.append('withMeta', 'true');
  url.searchParams.append('limit', '1');

  const response = await fetch(url.toString(), {
    method: 'GET',
    headers: new Headers({
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
      'Content-Type': 'application/json',
    }),
  });

  const data = await response.json();

  const item = data.items[0];

  const meta = item.meta;
  delete meta.content;
  const content = JSON.parse(item.content);

  return { meta, content };
};

export const quickSearch = async (
  text: string,
  token: string,
  adapterType: AdapterType = AdapterType.DATA
): Promise<CatalogSearchResults | CatalogSearchError> => {
  const searchTerms = text.split(' ').join(',');

  const adapterUrl =
    adapterType === AdapterType.DATA ? PUBLIC_DATA_ADAPTER_URL : PUBLIC_INFRA_ADAPTER_URL;

  try {
    const response = await fetch(`${adapterUrl}/api/v1/search/quick?searchString=${searchTerms}`, {
      method: 'POST',
      headers: new Headers({
        Accept: 'application/json',
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json',
      }),

      // not sure how text is passed
    });

    const data: RawCatalogSearchResults | CatalogSearchError = await response.json();

    if (isError(data)) {
      throw new Error(data.message);
    }

    // TODO: check and transform
    // we'll use the same transform from searchCatalogue as the response should in theory be the same
    // not sure about data.message or data.code

    return {
      items: transformSearchResultItems(data),
      totalCount: data.totalCount,
    };
  } catch (err) {
    return {
      code: 'error',
      message: (err as Error)?.message ?? 'Unknown error',
    };
  }
};

export const getSchemas = async () => {
  const token = await getKeycloakToken();
  const response = await fetch(`${PUBLIC_FC_DATA_URL}/schemas`, {
    method: 'GET',
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
  return response.json() as Promise<FetchSchemasResponse>;
};

export const fetchSchemaData = async (schema: string) => {
  const token = await getKeycloakToken();
  const response = await fetch(`${PUBLIC_FC_DATA_URL}/schemas/${schema}`, {
    method: 'GET',
    headers: {
      Authorization: `Bearer ${token}`,
      Accept: 'text/turtle',
    },
  });
  return response.text();
};

export const postDocument = async (document: string) => {
  const token = await getKeycloakToken();
  return fetch(`${PUBLIC_FC_DATA_URL}/documents`, {
    method: 'POST',
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

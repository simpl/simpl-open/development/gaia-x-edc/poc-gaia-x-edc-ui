import '@testing-library/jest-dom';
import { mount } from '@vue/test-utils';
import { describe, expect, it } from 'vitest';

import Dropzone from './Dropzone.vue';

describe('Dropzone component', () => {
  const wrapper = mount(Dropzone);
  const dropzone = wrapper.get('.dropzone');

  it('renders the component', () => {
    expect(dropzone.text()).toMatch(
      'Self description JSON-LD file: * Only json jsonld files are accepted. Maximum size 2 MBChoose file'
    );
  });
});

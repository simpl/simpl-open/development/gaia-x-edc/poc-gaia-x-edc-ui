export interface AccessPolicyPermission {
  assignee: string;
  action: string;
  fromDatetime?: string;
  toDatetime?: string;
}

export interface AccessPoliciesDTO {
  resourceUri: string;
  permissions: AccessPolicyPermission[];
}

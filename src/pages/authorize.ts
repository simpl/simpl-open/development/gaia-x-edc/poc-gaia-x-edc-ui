import type { APIRoute } from 'astro';
import { login } from '../services/keycloak';

// This route is used to keep the keycloak secret on server side, the env variable is only accessible there
export const POST: APIRoute = async ({ request }) => {
  const { username, password } = await request.json();
  if (!username || !password)
    return new Response(JSON.stringify({ error_description: 'Invalid user credentials' }), {
      status: 401,
    });

  return login(username, password);
};

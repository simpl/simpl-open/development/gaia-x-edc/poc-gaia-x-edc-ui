import clsx from 'clsx';
import { createRef, useCallback, useState } from 'react';
import { JSONTree } from 'react-json-tree';
import { signJson } from '../../api/signer';

const theme = {
  scheme: 'solarized',
  author: 'ethan schoonover (http://ethanschoonover.com/solarized)',
  base00: '#FFF',
  base01: '#073642',
  base02: '#586e75',
  base03: '#657b83',
  base04: '#839496',
  base05: '#93a1a1',
  base06: '#eee8d5',
  base07: '#fdf6e3',
  base08: '#dc322f',
  base09: '#cb4b16',
  base0A: '#b58900',
  base0B: '#859900',
  base0C: '#2aa198',
  base0D: '#268bd2',
  base0E: '#6c71c4',
  base0F: '#d33682',
};

export const SignJSON: React.FC = () => {
  const [jsonContent, setJsonContent] = useState<object>();
  const [signed, setSigned] = useState(false);
  const [hasError, setHasError] = useState<{ message: string; error: string } | undefined>();
  const fileInputRef = createRef<HTMLInputElement>();

  const handleFileChange = useCallback((event: React.ChangeEvent<HTMLInputElement>) => {
    setHasError(undefined);
    setSigned(false);

    const file = event.target.files?.[0];
    const reader = new FileReader();

    reader.onload = (e) => {
      let content: object | undefined = undefined;
      try {
        if (e.target?.result && typeof e.target.result === 'string') {
          content = JSON.parse(e.target.result);
        }
      } catch (error) {
        if (error && error instanceof Error) {
          setHasError({
            message: 'Error reading JSON file',
            error: error.message,
          });
        }
      }

      if (content) {
        setJsonContent(content);
      }
    };

    if (file) {
      reader.readAsText(file);
    }
  }, []);

  const handleSubmit = useCallback(
    (event: React.FormEvent<HTMLFormElement>) => {
      event.preventDefault();

      if (jsonContent) {
        signJson(jsonContent)
          .then((data) => {
            setSigned(true);
            setJsonContent(data);
          })
          .catch((error) => {
            setHasError({
              message: 'Error signing JSON',
              error: error.message,
            });
          });
      }
    },
    [jsonContent]
  );

  const handleReset = useCallback(() => {
    if (fileInputRef.current) {
      fileInputRef.current.value = '';
      fileInputRef.current.files = null;
      fileInputRef.current.type = 'text';
      fileInputRef.current.type = 'file';
    }
    setJsonContent(undefined);
    setSigned(false);
    setHasError(undefined);
  }, [fileInputRef]);

  const handleContentChange = useCallback(
    (event: React.ChangeEvent<HTMLTextAreaElement>) => {
      if (!hasError) {
        try {
          const parsedContent: object = JSON.parse(event.target.value);
          setJsonContent(parsedContent);
        } catch (err) {}
      }
    },
    [hasError]
  );

  const handleDownload = useCallback(() => {
    const blob = new Blob([JSON.stringify(jsonContent, null, 2)], { type: 'application/json' });
    const url = URL.createObjectURL(blob);
    const a = document.createElement('a');
    a.href = url;
    a.download = 'signed.json';
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
  }, [jsonContent]);

  return (
    <div className="mx-auto mt-4 flex flex-col items-center justify-center md:w-full xl:w-4/5">
      <div className="flex">
        <input type="file" onChange={handleFileChange} ref={fileInputRef} />
      </div>

      <form onSubmit={handleSubmit} className="flex w-full flex-col items-center">
        {
          <div className="flex w-full">
            {!hasError && (
              <textarea
                id="jsonContent"
                className={clsx(
                  'mx-auto my-5 h-80 w-full overflow-auto whitespace-pre-wrap rounded-md border border-gray-300 p-4 font-mono',
                  {
                    'bg-ec-status-success': signed,
                  }
                )}
                onChange={handleContentChange}
                value={jsonContent ? JSON.stringify(jsonContent, null, 2) : undefined}
                disabled={signed || !!hasError}
              />
            )}
            {hasError && (
              <textarea
                id="error"
                className={clsx(
                  'mx-auto my-5 h-80 w-full overflow-auto whitespace-pre-wrap rounded-md border border-gray-300 bg-ec-status-error p-4 font-mono'
                )}
                defaultValue={JSON.stringify(hasError, null, 2)}
              />
            )}
          </div>
        }
        <div>
          <button
            type="submit"
            disabled={signed || !!hasError}
            className="m-4 rounded-md bg-ec-primary p-3 font-bold text-white"
          >
            Sign JSON
          </button>
          <button
            onClick={handleReset}
            className="m-4 rounded-md bg-ec-secondary p-3 font-bold text-black"
          >
            Reset
          </button>
          {signed && (
            <button
              onClick={handleDownload}
              className="m-4 rounded-md bg-ec-status-success p-3 font-bold"
            >
              Download
            </button>
          )}
        </div>
      </form>
      {jsonContent && signed && (
        <div className="flex">
          <JSONTree
            data={jsonContent}
            theme={{
              extend: theme,
              // @ts-ignore
              nestedNodeLabel: ({ style }, keyPath: string[]) => {
                const highlight =
                  keyPath.find((key) => key === 'proof' || key === 'issuer') &&
                  !keyPath.some((key) => key === 'credentialSubject');
                return {
                  style: {
                    ...style,
                    color: highlight ? 'red' : 'black',
                  },
                };
              },
            }}
            shouldExpandNodeInitially={(kp, data, level) => {
              // return true if kp array of strings contain 'proof'

              return kp.some((key) => {
                if (key === 'proof' && level === 1) {
                  return true;
                }
                if (level === 0) {
                  return true;
                }
                return false;
              });
            }}
          />
        </div>
      )}
    </div>
  );
};

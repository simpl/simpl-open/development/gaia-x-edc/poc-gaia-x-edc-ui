/// <reference types="vitest" />
import react from '@vitejs/plugin-react';
import { getViteConfig } from 'astro/config';

export default getViteConfig({
  plugins: [react()],
  test: {
    include: '**/*.{test,spec}.?(c|m)[jt]s?(x)',
    exclude: ['e2e', 'node_modules', 'coverage', 'dist', 'build', 'public'],
    environment: 'jsdom',
    globals: true,
    setupFiles: './testSetup.js',
    coverage: {
      reportsDirectory: './coverage',
      provider: 'istanbul',
      reporter: ['json', 'lcov', 'html'],
    },
  },
});

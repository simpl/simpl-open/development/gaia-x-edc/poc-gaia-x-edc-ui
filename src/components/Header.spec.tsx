import '@testing-library/jest-dom';
import { render } from '@testing-library/react';
import { describe, it } from 'vitest';
import HeaderComponent from './Header';

describe.concurrent('Header Component', () => {
  it('should render', ({ expect }) => {
    const title = 'vitest header component';
    const { getByText } = render(<HeaderComponent title={title} searchValue={''} />);
    expect(getByText(title)).toBeInTheDocument();
    // test code here
  });
});

import jsonld from 'jsonld';
import { DataFactory, Store } from 'n3';

const { namedNode, literal, quad } = DataFactory;

export const createRdfRepresentation = async (
  data: Record<string, any>,
  context: Record<string, string>
): Promise<any> => {
  const store = new Store();

  const addToStore = (subject: string, predicate: string, object: any) => {
    if (typeof object === 'object' && object !== null) {
      if (Array.isArray(object)) {
        object.forEach((item) => {
          store.addQuad(namedNode(subject), namedNode(predicate), literal(item));
        });
      } else if ('@id' in object) {
        store.addQuad(namedNode(subject), namedNode(predicate), namedNode(object['@id']));
      } else if ('@value' in object && '@type' in object) {
        store.addQuad(
          namedNode(subject),
          namedNode(predicate),
          literal(object['@value'], namedNode(object['@type']))
        );
      } else {
        const blankNodeId = `_:b${Math.random().toString(36).substring(2, 15)}`;
        store.addQuad(namedNode(subject), namedNode(predicate), namedNode(blankNodeId));
        Object.entries(object).forEach(([key, value]) => {
          addToStore(blankNodeId, key, value);
        });
      }
    } else {
      store.addQuad(namedNode(subject), namedNode(predicate), literal(object));
    }
  };

  const processObject = (obj: Record<string, any>, parentSubject: string) => {
    Object.entries(obj).forEach(([key, value]) => {
      if (key !== '@context' && key !== '@id') {
        addToStore(parentSubject, key, value);
      }
    });
  };

  processObject(data, data['@id']);

  const jsonLdArray = store.getQuads(null, null, null, null).map((quad) => ({
    subject: { termType: quad.subject.termType, value: quad.subject.value },
    predicate: { termType: quad.predicate.termType, value: quad.predicate.value },
    object: {
      termType: quad.object.termType,
      value: quad.object.value,
      ...(quad.object.termType === 'Literal' && quad.object.datatype
        ? { datatype: { termType: 'NamedNode', value: quad.object.datatype.value } }
        : {}),
    },
  }));

  const jsonLdObj = {
    '@context': context,
    '@graph': jsonLdArray,
  };

  return jsonld.compact(jsonLdObj, context);
};

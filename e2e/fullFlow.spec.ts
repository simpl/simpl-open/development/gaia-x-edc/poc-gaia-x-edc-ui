import { expect, test } from '@playwright/test';
import { readFileSync } from 'fs';
import { BASE_URL, SD_TOOL_URL, dataOfferingFormSelectors } from './config';

const TEST_USER = 'user';
const TEST_PASSWORD = 'User!!!!!!!1';

const authFile = 'playwright/.auth/user.json';
const sdOfferingFile = 'playwright/sdOffering.json';
const signedOfferingFile = 'playwright/signedOffering.json';

// generate random hex value
const randomHex = Math.random().toString(16).slice(2, 10);

test.describe('Login', () => {
  test('Should be able to login with test user', async ({ page }) => {
    await page.goto(`${BASE_URL}/upload`);

    // wait for the redirect, we wait until network idle because the content is server rendered
    // but full interactivity only happens after JS load - which we need to somehow address
    await page.waitForURL(`**/login`, {
      waitUntil: 'networkidle',
    });

    // fill login form input "username and input "password"
    await page.locator('input[name="username"]').fill(TEST_USER);

    await page.locator('input[name="password"]').fill(TEST_PASSWORD);

    // click button with text 'Sign In'
    await page.getByText('Sign In').click();

    await page.waitForURL(`**/upload`, {
      waitUntil: 'networkidle',
    });

    await page.context().storageState({ path: authFile });

    await expect(page).toHaveTitle('Publish to catalogue');
    await expect(page.getByText('User logged in')).toBeVisible();
  });
});

test.describe('Full operation flow', async () => {
  test.use({ storageState: authFile });

  let signedContent = {};

  test('Should be able to use sd-tooling to create a service offering', async ({ page }) => {
    // go to main page of sd-tool
    await page.goto(SD_TOOL_URL, {
      waitUntil: 'networkidle',
    });

    await page.waitForLoadState('load');

    // click button with text 'Get Started'
    await page.locator('button:has-text("Get Started")').click();

    // wait until navigation is complete
    await page.waitForURL(`**/select-file`, {
      waitUntil: 'networkidle',
    });

    // select SIMPL schema from ecosystem
    await page.hover('div.dropdown');

    await page.locator('a:has-text("SIMPL")').click();

    await expect(page.locator('button:has-text("Data Offering")')).toBeVisible();
    await expect(page.locator('button:has-text("Application Offering")')).toBeVisible();

    // click button with text 'Data Offering'
    await page.locator('button:has-text("Data Offering")').click();

    await page.waitForResponse('**/getJSON**');

    await page.waitForURL(`**/form`, {
      waitUntil: 'networkidle',
    });

    await page.waitForTimeout(1000);

    // check that we're on the data-offering page
    await expect(page.locator('h2:has-text("Complete DataOffering Form")')).toBeVisible();

    // fill in the form
    for (const key of Object.keys(dataOfferingFormSelectors)) {
      const tagSelector = dataOfferingFormSelectors[key].tag;
      await page.locator(tagSelector).click();

      const keysWithoutTag = Object.keys(dataOfferingFormSelectors[key]).filter((k) => k !== 'tag');

      for (const keyWithoutTag of keysWithoutTag) {
        const inputSelector = dataOfferingFormSelectors[key][keyWithoutTag];
        const input = page.locator(inputSelector);
        await expect(input).toBeVisible();
        if (keyWithoutTag === 'price') {
          input.fill('1234');
        } else {
          input.fill(randomHex);
        }
      }
    }

    const jsonLDRadio = page.locator('mat-radio-button:has-text("JSON-LD")');

    await expect(jsonLDRadio).toBeVisible();

    await jsonLDRadio.click();

    const saveButton = page.locator('button:has-text("Save")');

    await expect(saveButton).toBeVisible();

    const downloadPromise = page.waitForEvent('download');

    await saveButton.click();

    const download = await downloadPromise;

    await download.saveAs(sdOfferingFile);
  });

  test('Should be able to sign a offering as provided from SD-tooling', async ({ page }) => {
    await page.goto(`${BASE_URL}/sign`, {
      waitUntil: 'networkidle',
    });

    await expect(page.locator('input[type="file"]')).toBeVisible();

    await page.locator('input[type="file"]').setInputFiles(sdOfferingFile);

    // wait for response from server request
    const signerPromise = page.waitForResponse('**/signjson');

    await page.locator('button:has-text("Sign JSON")').click();

    const resp = await signerPromise;

    signedContent = await resp.json();

    await expect(page.getByText('root:')).toBeVisible();

    const downloadButton = page.locator('button:has-text("Download")');

    await expect(downloadButton).toBeVisible();

    const downloadPromise = page.waitForEvent('download');

    await downloadButton.click();

    const download = await downloadPromise;

    await download.saveAs(signedOfferingFile);
  });

  test('Should be able to publish signed file to catalogue', async ({ page }) => {
    await page.goto(`${BASE_URL}/upload`, {
      waitUntil: 'networkidle',
    });

    await page.locator('input[name="fileupload"]').setInputFiles(signedOfferingFile);

    const clickButton = page.locator('button:has-text("Publish to catalogue")');

    await expect(clickButton).toBeVisible();

    await clickButton.click();

    await page.waitForLoadState('networkidle');

    await expect(page.getByText('Publishing successful')).toBeVisible();
  });

  test('Should be able to find offering in catalogue', async ({ page }) => {
    const searchQuery = `MATCH (node) WHERE any(prop IN ['title', 'description', 'location', 'identifier'] WHERE node[prop] CONTAINS $searchstring) RETURN node`;

    await page.goto(`${BASE_URL}/search`, {
      waitUntil: 'networkidle',
    });

    const searchSelector = 'input[id="search-resource"]';

    await page.locator(searchSelector).click();

    const optionLocatior = page.getByText('MATCH (node) WHERE any(prop');

    await expect(optionLocatior).toBeVisible();

    await optionLocatior.click();

    await expect(page.locator('input[name="searchstring"]')).toBeVisible();

    const signedOffering = JSON.parse(readFileSync(signedOfferingFile, 'utf-8'));

    // identifier property from offering that we'll be using for searching
    const identifier =
      signedOffering['credentialSubject']['simpl:generalServiceProperties']['simpl:identifier'][
        '@value'
      ];

    await page.locator('input[name="searchstring"]').fill(identifier);

    await page.locator('button:has-text("Search")').click();

    await expect(page.getByText(identifier).nth(1)).toBeVisible();
  });
});
